gst-plugins-espeak (0.5.0-1) unstable; urgency=medium

  [ upstream ]
  * New release.
    + Port to espeak-ng 1.49, but keep compatibile with espeak 1.48.
    + Fix data type of pitch and rate get property.

  [ Jonas Smedegaard ]
  * Set Vcs-* to salsa.debian.org.
  * Declare compliance with Debian Policy 4.2.1.
  * Update watch file:
    + Fix typo in usage comment.
    + Use https protocol.
  * Update copyright info:
    + Update preferred upstream contract and alternate source URLs to
      reference Github resources.
    + Extend coverage for autotools.
  * Drop patch 1001: Fixed upstream.
  * Add patches cherry-picked upstream:
    + Fix an event misordering bug.
    + Stop using -export-symbols-regex (replacing local patch).
  * Acknowledge NMU.
    Thanks to Adrian Bunk.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 25 Oct 2018 17:46:05 +0200

gst-plugins-espeak (0.4.0-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS with GStreamer 1.14. (Closes: #894642)

 -- Adrian Bunk <bunk@debian.org>  Sat, 28 Apr 2018 20:07:37 +0300

gst-plugins-espeak (0.4.0-3) unstable; urgency=medium

  * Add patch 1001 to set "english" (not "default") as default voice, to
    properly work with espeak-ng.
    Closes: Bug#877750. Thanks to James Cameron.
  * Update watch file:
    + Bump to file format 4.
    + Mention gbp --uscan in usage comment.
    + Use substitution strings.
  * Modernize git-buildpackage config: Fix filter any .git* file.
  * Modernize Vcs-* fields:
    + Consistently use https (not git) protocol.
    + Consistently use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.1.1.
  * Update copyright info:
    + Strip superfluous copyright signs.
    + Use SPDX License shortnames FSFUL FSFULLR.
    + Sort License sections alphabetically.
    + Use https protocol in file format URL.
    + Extend coverage for myself.
  * Modernize cdbs:
    + Drop get-orig-source target: Use gbp import-orig --uscan.
    + Do copyright-check in maintainer script (not during build).
    + Stop build-depend on licensecheck.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 05 Oct 2017 08:57:39 +0200

gst-plugins-espeak (0.4.0-2) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.8.
  * Update copyright info: Extend coverage for Debian packaging.
  * Git-ignore quilt .pc dir.
  * Use dh-autoreconf.
    Build-depend on dh-autoreconf and autoconf-archive.
  * Use eSpeak NG instead of eSpeak:
    + Build-depend on libespeak-ng-libespeak-dev, and libespeak-dev only
      as fallback.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 14 Dec 2016 00:19:19 +0100

gst-plugins-espeak (0.4.0-1) unstable; urgency=low

  * Initial release.
    Closes: bug#789974.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 25 Jun 2015 18:28:36 -0500
